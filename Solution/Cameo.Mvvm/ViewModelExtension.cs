﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Cameo.Mvvm
{
    public static class ViewModelExtension
    {
        /// <summary>
        /// Checks if a property already matches a desired value. Sets the property and
        /// notifies listeners only when necessary.
        /// </summary>
        /// <typeparam name="T">Type of the property.</typeparam>
        /// <param name="storage">Reference to a property with both getter and setter.</param>
        /// <param name="value">Desired value for the property.</param>
        /// <param name="propertyName">Name of the property used to notify listeners. This
        /// value is optional and can be provided automatically when invoked from compilers that
        /// support CallerMemberName.</param>
        /// <returns>True if the value was changed, false if the existing value matched the
        /// desired value.</returns>
        public static bool Update<T>(this ViewModelBase vm, ref T storage, T value, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(storage, value)) return false;

            storage = value;
            vm.RaisePropertyChanged(propertyName);

            return true;
        }

        /// <summary>
        /// Checks if a property already matches a desired value. Sets the property and
        /// notifies listeners only when necessary.
        /// </summary>
        /// <typeparam name="T">Type of the property.</typeparam>
        /// <param name="storage">Reference to a property with both getter and setter.</param>
        /// <param name="value">Desired value for the property.</param>
        /// <param name="propertyName">Name of the property used to notify listeners. This
        /// value is optional and can be provided automatically when invoked from compilers that
        /// support CallerMemberName.</param>
        /// <param name="onChanging">Action that is called before the property value has been changed.</param>
        /// <param name="onChanged">Action that is called after the property value has been changed. But before the event <see cref="ViewModelBase.PropertyChanged"/> invoke.</param>
        /// <returns>True if the value was changed, false if the existing value matched the
        /// desired value.</returns>
        public static bool Update<T>(this ViewModelBase vm, ref T storage, T value, Action onChanging, Action onChanged, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(storage, value)) return false;

            onChanging?.Invoke();
            storage = value;
            onChanged?.Invoke();
            vm.RaisePropertyChanged(propertyName);

            return true;
        }
    }
}